import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ModuleConfig {

    public static PROTOCOL: string = "http";
    public static HOST: string = "localhost";
    public static PORT: string = "8080";
    public static BASE_URL: string = ModuleConfig.PROTOCOL + "://" + ModuleConfig.HOST + ":" + ModuleConfig.PORT;


    public static readonly REGISTERATION: string = ModuleConfig.BASE_URL + "/user/add";
    public static readonly LOGIN: string = ModuleConfig.BASE_URL + "/user/login/?userName={userName}&userPasswd={userPasswd}";

    public static readonly PET_SAVE: string = ModuleConfig.BASE_URL + "/pets/addPet";
    public static readonly PET_UPDATE: string = ModuleConfig.BASE_URL + "/pets/updatePetDetails/";
    public static readonly PET_BY_ID: string = ModuleConfig.BASE_URL + "/pets/petDetail/";
    public static readonly PET_LIST: string = ModuleConfig.BASE_URL + "/pets/allPets";
    public static readonly PET_BUY: string = ModuleConfig.BASE_URL + "/user/pets/buyPet/{userId}/{petId}";
    public static readonly USER_PETS: string = ModuleConfig.BASE_URL + "/user/pets/myPets/";

}