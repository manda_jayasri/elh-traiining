import { Component } from '@angular/core';
import { Menu } from './Components/menu/menu';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'petpeers-web';

  items: Menu[] = [
    {
      displayName: "Home",
      name: "Home",
      url: "/home",
      secure: false
    }, {
      displayName: "Login",
      name: "Login",
      url: "/login",
      secure: false
    }, {
      displayName: "User Register",
      name: "User Register",
      url: "/registration",
      secure: false
    },
    {
      displayName: "Home",
      name: "Pets data",
      url: "/pet-list",
      secure: true
    }, {
      displayName: "My Pets",
      name: "My Pets",
      url: "/my-pets",
      secure: true
    },
    {
      displayName: "Add Pet",
      name: "Add Pet",
      url: "/pet-add",
      secure: true
    }

  ]


}
