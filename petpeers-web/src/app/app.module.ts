import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { MenuComponent } from './Components/menu/menu.component';
import { LoginComponent } from './Components/login/login.component';
import { PetAddComponent } from './Components/pet-add/pet-add.component';
import { UserRegistrationComponent } from './Components/user-registration/user-registration.component';
import { PetListComponent } from './Components/pet-list/pet-list.component';
import { HomeComponent } from './Components/home/home.component';
import { AppRoutingModule } from './app.routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { UserPetsComponent } from './Components/user-pets/user-pets.component';
import { PetEditComponent } from './Components/pet-edit/pet-edit.component';
import { PetResolver } from './Components/pet-list/pet.resolver';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    LoginComponent,
    UserRegistrationComponent,
    PetAddComponent,
    PetListComponent,
    HomeComponent,
    UserPetsComponent,
    PetEditComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [PetResolver],
  bootstrap: [AppComponent]
})
export class AppModule { }
