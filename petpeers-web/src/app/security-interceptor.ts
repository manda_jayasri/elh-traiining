import { HttpInterceptor, HttpEvent, HttpRequest, HttpHandler } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ModuleConfig } from 'src/app/config/module-config';
@Injectable()
export class SecurityInterceptor implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let item: string = sessionStorage.getItem('user');
        if (item && item.length > 0 && request.url.indexOf(":" + ModuleConfig.PORT) != -1) {
            let user: any = JSON.parse(item);
            console.log("Username: " + user.username);
            var auth: string = user.username + ":" + user.password;
            request = request.clone({
                setHeaders: {
                    Authorization: `Basic ${window.btoa(auth)}`
                }
            });
        }
        return next.handle(request);
    }
}