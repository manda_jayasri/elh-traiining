export class Menu {
    name: string | undefined;
    displayName: string | undefined;
    url: string | undefined;
    secure: boolean | undefined;
    subItems?: Menu[] = [];
}