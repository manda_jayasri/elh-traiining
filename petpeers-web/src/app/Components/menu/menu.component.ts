import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Menu } from './menu';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  @Input() items: Menu[] = [];

  user: any = {};

  constructor(private router:Router){}

  ngOnInit() {
  }

  scroll(el: HTMLElement) {
    el.scrollIntoView();
  }

  public isUserLoggedin(): boolean {
    return (sessionStorage.getItem('user') != undefined);
  }

  public hasAccess(menu: Menu): boolean {
    var allow: boolean = false;
    var user: string | null = sessionStorage.getItem('user');

    if (!user) {
      if (menu.secure === false) {
        allow = true;
      }
    } else {
      if (user) {
        var userObj: any = JSON.parse(user);
        var self = this;
        if (userObj != null && menu && menu.name) {
          if (menu.secure === true) {
            allow = true;
          }
        }
        this.user = userObj;
      }
    }
    return allow;
  }


  public logout() {
    let answer: boolean = confirm("Are you sure you want to logout? Click OK to confirm.");
    if (answer) {
      let user: string | null | undefined = sessionStorage.getItem('user');
      if (user != null) {
        sessionStorage.removeItem('user');
        this.router.navigate(['/home']);
      }
    } else {
    }
  }

  ToggleNavBar() {
    let element: HTMLElement = document.getElementsByClassName('navbar-toggler')[0] as HTMLElement;
    if (element.getAttribute('aria-expanded') == 'true') {
      element.click();
    }
  }

}
