import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PetService } from '../pet-list/pet.service';

@Component({
  selector: 'app-pet-add',
  templateUrl: './pet-add.component.html',
  styleUrls: ['./pet-add.component.css']
})
export class PetAddComponent implements OnInit {

  petForm: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private petService: PetService,
  ) {

  }

  ngOnInit() {
    this.petForm = this.formBuilder.group({
      name: ['', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(30),
        Validators.pattern('^[a-zA-Z ]*$')]],
      age: ['', [
        Validators.required,
        Validators.maxLength(2),
        Validators.minLength(1),
        Validators.pattern('^[0-9]*$')]],
      place: ['', [
        Validators.required,
        Validators.maxLength(255)
      ]]
    });
  }

  // convenience getter for easy access to form fields
  get p() { return this.petForm.controls; }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.petForm.invalid) {
      return;
    }
    var pet: any = this.petForm.value;
    var user: string | null = sessionStorage.getItem('user');
    if (user) {
      var data = JSON.parse(user);
      pet.user = data;
    }
    this.petService.savePetData(this.petForm.value).subscribe(data => {
      alert('Pet Added successful');
      this.router.navigate(['/pet-list']);
    }, (response) => {
      alert("Error: "+response.error.message);
    });
  }



}
