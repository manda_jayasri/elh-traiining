import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { PetService } from '../pet-list/pet.service';

@Component({
  selector: 'app-pet-edit',
  templateUrl: './pet-edit.component.html',
  styleUrls: ['./pet-edit.component.css']
})
export class PetEditComponent implements OnInit {
  petForm: FormGroup;
  submitted = false;
  data: any;
  pet:any;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private petService: PetService,
  ) { }

  ngOnInit() {
    this.data = this.route.snapshot.data;
    this.petForm = this.formBuilder.group({
      name: ['', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(30),
        Validators.pattern('^[a-zA-Z ]*$')]],
      age: ['', [
        Validators.required,
        Validators.maxLength(2),
        Validators.minLength(1),
        Validators.pattern('^[0-9]*$')]],
      place: ['', [
        Validators.required,
        Validators.maxLength(255)
      ]],
    });
    this.pet=this.data.pet;
    this.petForm.setValue({
      name: this.pet.name, 
      age: this.pet.age,
      place:this.pet.place
    });
  }

  // convenience getter for easy access to form fields
  get p() { return this.petForm.controls; }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.petForm.invalid) {
      return;
    }
    var pet: any = this.petForm.value;
    var user: string | null = sessionStorage.getItem('user');
    if (user) {
      var data = JSON.parse(user);
      pet.user = data;
    }
    this.petService.updatePetData(this.petForm.value,this.pet.id).subscribe(data => {
      alert('Pet Updated successful');
      this.router.navigate(['/my-pets']);
    }, (response) => {
      alert("Error: " + response.error.message);
    });
  }

}
