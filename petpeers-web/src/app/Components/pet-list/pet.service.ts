import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ModuleConfig } from 'src/app/config/module-config';


@Injectable({
  providedIn: 'root'
})
export class PetService {

  constructor(private httpClient: HttpClient) { }

  public savePetData(pet: any) {
    return this.httpClient.post(ModuleConfig.PET_SAVE, pet);
  }

  public updatePetData(pet: any, petId: number) {
    return this.httpClient.put(ModuleConfig.PET_UPDATE + petId, pet);
  }

  public petList() {
    return this.httpClient.get(ModuleConfig.PET_LIST);
  }

  public petDataById(id) {
    return this.httpClient.get(ModuleConfig.PET_BY_ID + id);
  }

  public buyPet(userId, petId) {
    let url: string = ModuleConfig.PET_BUY.replace("{userId}", userId);
    url = url.replace("{petId}", petId);
    return this.httpClient.get(url);
  }

}