import { Component, OnInit } from '@angular/core';
import { PetService } from './pet.service';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pet-list',
  templateUrl: './pet-list.component.html',
  styleUrls: ['./pet-list.component.css']
})
export class PetListComponent implements OnInit {

  pets: any | null;
  user:any;

  constructor(private petService: PetService, private router: Router) { }

  ngOnInit() {
    this.petService.petList().subscribe((pets: any) => {
      this.pets = pets;
    }, (response: any) => {
      alert("Error: " + response.error.message);
    })
  }

  public buyPet(pet, i) {
    let answer: boolean = confirm("Are you sure you want to buy pet: " + pet.petName + " ? Click OK to confirm.");
    if (answer) {
      let user: string | null | undefined = sessionStorage.getItem('user');
      if (user != null) {
        var data = JSON.parse(user);
        this.petService.buyPet(data.id, pet.id).subscribe((data: any) => {
          alert("Bought Successfully.");
          this.router.navigate(['/my-pets']);
        }, (response: any) => {
          alert("Error: " + response.error.message);
        })
      } else { }
    }
  }

}