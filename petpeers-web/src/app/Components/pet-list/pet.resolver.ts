import { Injectable } from "@angular/core";
import { PetService } from "./pet.service";
import { ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { ModuleConfig } from "src/app/config/module-config";

@Injectable({ providedIn: 'root' })
export class PetResolver implements Resolve<any> {
    constructor(private httpClient: HttpClient) { }
    resolve(route: ActivatedRouteSnapshot) {
        var id: number = Number.parseInt(route.params['id']);
        return this.httpClient.get(ModuleConfig.PET_BY_ID + id);
    }
}