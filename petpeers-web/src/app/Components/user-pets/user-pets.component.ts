import { Component, OnInit } from '@angular/core';
import { UserService } from '../user-registration/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-pets',
  templateUrl: './user-pets.component.html',
  styleUrls: ['./user-pets.component.css']
})
export class UserPetsComponent implements OnInit {

  pets: any | undefined;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    let user: string | null | undefined = sessionStorage.getItem('user');
    if (user != null) {
      var data = JSON.parse(user);
      this.userService.userPets(data.id).subscribe((pets: any) => {
        this.pets = pets;
      }, (response: any) => {
        alert("Error: " + response.error.message);
      })
    }
  }

}