import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;
  public submitted = false;

  constructor(private formBuilder: FormBuilder, private router: Router,
    private loginService: LoginService) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      userName: ['', Validators.required],
      userPasswd: ['', Validators.required],
    });
  }

  get l() {
    return this.loginForm.controls;
  }

  onLogin() {
    // console.log(this.loginForm.value);
    this.submitted = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    if (this.loginForm.valid) {
      this.loginService.login(this.loginForm.value).subscribe((response: any) => {
       // console.log(JSON.stringify(response))
        sessionStorage.setItem("user", JSON.stringify(response));
        this.router.navigate(["/pet-list"]);
      }, (response: any) => {
        alert("Error: "+response.error.message);
      });
    }
  }

  public reset() {
    //this.loginForm = undefined;
  }

}