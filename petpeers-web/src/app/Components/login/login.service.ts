import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ModuleConfig } from 'src/app/config/module-config';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private httpClient: HttpClient) { }

  public login(user: any) {
    let url: string = ModuleConfig.LOGIN.replace("{userName}", user.userName);
    url = url.replace("{userPasswd}", user.userPasswd);
    // Make the API call using the new parameters.
    return this.httpClient.get(url);
  }

}
