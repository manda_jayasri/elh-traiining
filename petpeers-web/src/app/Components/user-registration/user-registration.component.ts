import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { MustMatch } from './must-match.validator';
import { UserService } from './user.service';

@Component({
  selector: 'app-user-registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.css']
})
export class UserRegistrationComponent implements OnInit {

  registerForm: FormGroup;
  loading = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService,
  ) {

  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      userName: ['', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(30),
        Validators.pattern('^[a-zA-Z ]*$')]],
      userPassword: ['', [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(12)]],
      confirmPassword: ['', [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(12)]]
    }, {
      validator: MustMatch('userPassword', 'confirmPassword')
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    this.userService.userRegisteration(this.registerForm.value).subscribe(data => {
      alert('Registration successful');
      this.router.navigate(['/login']);
    }, (response) => {
      alert("Error: "+response.error.message);
    });
  }

}
