import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ModuleConfig } from 'src/app/config/module-config';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) { }

  public userRegisteration(user: any) {
    return this.httpClient.post(ModuleConfig.REGISTERATION, user);
  }

  public userPets(userId:Number) {
    return this.httpClient.get(ModuleConfig.USER_PETS + userId);
  }


}
