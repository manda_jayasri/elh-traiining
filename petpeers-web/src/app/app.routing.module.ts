import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './Components/home/home.component';
import { UserRegistrationComponent } from './Components/user-registration/user-registration.component';
import { LoginComponent } from './Components/login/login.component';
import { PetAddComponent } from './Components/pet-add/pet-add.component';
import { PetListComponent } from './Components/pet-list/pet-list.component';
import { UserPetsComponent } from './Components/user-pets/user-pets.component';
import { PetEditComponent } from './Components/pet-edit/pet-edit.component';
import { PetResolver } from './Components/pet-list/pet.resolver';

const routes: Routes = [

    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    { path: 'registration', component: UserRegistrationComponent },
    { path: 'login', component: LoginComponent },
    { path: 'pet-add', component: PetAddComponent },
    { path: 'pet-list', component: PetListComponent },
    { path: 'my-pets', component: UserPetsComponent },
    {
        path: 'pet-edit/:id',
        component: PetEditComponent,
        resolve: {
          pet: PetResolver
        }
      }
];
@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }