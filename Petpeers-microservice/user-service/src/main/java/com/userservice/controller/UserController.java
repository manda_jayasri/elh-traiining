package com.userservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.userservice.dto.PetDto;
import com.userservice.exception.CustomException;
import com.userservice.model.User;
import com.userservice.service.UserService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(value = "user")
@Slf4j
public class UserController {

	@Autowired
	private UserService userService;
	
	@GetMapping(value = "/hello")
	public ResponseEntity<String> sayHello() {
		String str = userService.sayHello();
		return new ResponseEntity<String>("Welcome to User service and then called "+str + " ", HttpStatus.OK);
	}

	@PostMapping(value = "/create")
	public ResponseEntity<User> addUser(@RequestBody User user) {
		log.info("Called Add User Successfully");
		if (user != null)
			try {
				userService.saveUser(user);
				log.info("Done Add User Successfully ");
				return new ResponseEntity<User>(user, HttpStatus.CREATED);
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<User>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
	}

	@GetMapping(value = "pets/myPets/{userId}")
	public ResponseEntity<List<PetDto>> getMyPets(@PathVariable Long userId) throws CustomException {
		log.info("Called get PetDto by User id..");
		List<PetDto> petDtos = userService.getMyPets(userId);
		if (!petDtos.isEmpty()) {
			log.info("Found Pets data Successfully ");
			return new ResponseEntity<List<PetDto>>(petDtos, HttpStatus.OK);
		}
		return new ResponseEntity<List<PetDto>>(HttpStatus.NOT_FOUND);
	}

	@GetMapping(value = "buyPet/{userId}/{petId}")
	public ResponseEntity<User> buyPet(@PathVariable Long userId, @PathVariable Long petId) throws CustomException {
		log.info("Called Buy pet method..");
		User user = userService.buyPet(userId, petId);
		if (user != null) {
			log.info("Bougt PetDto to User Successfully ");
			return new ResponseEntity<User>(user, HttpStatus.OK);
		}
		return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
	}

	@GetMapping(value = "/getUser/{id}")
	public ResponseEntity<User> getUserById(@PathVariable("id") Long id) throws CustomException {
		log.info("Called serach User method ");
		User user = userService.getUserById(id);
		if (user != null) {
			log.info("Got User Successfully ");
			return new ResponseEntity<User>(user, HttpStatus.OK);
		}
		return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
	}

	@GetMapping(value = "/allUsers")
	public ResponseEntity<List<User>> getAllUsers() throws CustomException {
		log.info("Callet get all User methpd ");
		List<User> users = userService.getAllUsers();
		if (!users.isEmpty()) {
			log.info("Found Users data Successfully ");
			return new ResponseEntity<List<User>>(users, HttpStatus.OK);
		}
		return new ResponseEntity<List<User>>(HttpStatus.NOT_FOUND);
	}

	@PutMapping(value = "/updateUser/{id}")
	public ResponseEntity<User> updateUser(@RequestBody User user, @PathVariable Long id) {
		log.info("Called update User ");
		if (user != null && id != null)
			try {
				userService.updateUser(user, id);
				log.info(" Update User Done Successfully ");
				return new ResponseEntity<User>(user, HttpStatus.OK);
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<User>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
	}

	@PostMapping(value = "/delete/{id}")
	public ResponseEntity<Boolean> removeUser(@PathVariable Long id) {
		if (id != null)
			try {
				Boolean res = userService.deleteUser(id);
				log.info(" Remove User Done Successfully ");
				return new ResponseEntity<Boolean>(res, HttpStatus.GONE);
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		return new ResponseEntity<Boolean>(HttpStatus.BAD_REQUEST);
	}

}
