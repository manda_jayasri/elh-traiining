package com.userservice.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.userservice.model.User;
@Repository
public interface UserRepository extends CrudRepository<User, Long> {

	User findByUserName(String userName);

}
