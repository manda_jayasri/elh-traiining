package com.userservice.dto;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.userservice.model.User;

import lombok.Data;

@Data
@Component
public class PetDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	private String name;

	private String place;

	private Integer age;

	private User user;
}
