package com.userservice.feignclient;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.userservice.dto.PetDto;
import com.userservice.model.User;

@FeignClient(name = "PET-SERVICE/pets")
public interface PetClient {

	@PutMapping(value = "/buyPet/{petId}")
	public PetDto buyPetByUserId(@PathVariable(value="petId") Long petId, @RequestBody User user);

	@GetMapping(value = "/allPetsByUserId/{userId}")
	public List<PetDto> getAllPetByUserId(@PathVariable("userId") Long userId);

	@GetMapping(value = "/hello")
	public String sayHello();

}