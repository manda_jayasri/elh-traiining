package com.userservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.userservice.dto.PetDto;
import com.userservice.exception.CustomException;
import com.userservice.feignclient.PetClient;
import com.userservice.model.User;
import com.userservice.repository.UserRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PetClient petClient;
	
	@Override
	public String sayHello() {
		return petClient.sayHello();
	}

	@Override
	public User saveUser(User user) throws CustomException {
		log.info("Called add new User in service Successfully ");
		User dbUser = userRepository.findByUserName(user.getUserName());
		if (dbUser != null) {
			throw new CustomException(" User data already exists with name: " + user.getUserName());
		}
		return userRepository.save(user);
	}

	@Override
	public List<PetDto> getMyPets(Long userId) throws CustomException {
		User dbUser = userRepository.findById(userId).get();
		if (dbUser == null) {
			throw new CustomException(" User data doesnot exists with id: " + userId);
		}
		return petClient.getAllPetByUserId(dbUser.getId());
	}

	@Override
	public User buyPet(Long userId, Long petId) throws CustomException {
		User dbUser = userRepository.findById(userId).get();
		if (dbUser == null) {
			throw new CustomException(" User data doesn't exists with id: " + userId);
		}
		petClient.buyPetByUserId(petId,dbUser);
		return dbUser;
	}

	@Override
	public List<User> getAllUsers() throws CustomException {
		log.info("Called find all Users in service Successfully ");
		return (List<User>) userRepository.findAll();
	}

	@Override
	public User getUserById(Long id) throws CustomException {
		log.info("Called get User data in service ");
		User dbUser = userRepository.findById(id).get();
		if (dbUser == null) {
			throw new CustomException(" User data doesnot exists with id: " + id);
		}
		return dbUser;
	}

	@Override
	public User updateUser(User user, Long id) throws CustomException {
		log.info("Called update User in service ");
		User dbUser = userRepository.findById(id).get();
		if (dbUser == null) {
			throw new CustomException(" User data doesnot exists with id: " + id);
		}
		dbUser.setUserName(user.getUserName());
		dbUser.setUserPassword(user.getUserPassword());
		dbUser.setConfirmPassword(user.getConfirmPassword());
		return userRepository.save(dbUser);
	}

	@Override
	public Boolean deleteUser(Long id) throws CustomException {
		log.info("Called delete User in service ");
		User dbUser = userRepository.findById(id).get();
		if (dbUser == null) {
			throw new CustomException(" User data doesnot exists with id: " + id);
		}
		userRepository.delete(dbUser);
		return true;
	}

	

}
