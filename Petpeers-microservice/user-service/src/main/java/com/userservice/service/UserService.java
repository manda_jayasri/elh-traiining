package com.userservice.service;

import java.util.List;

import com.userservice.dto.PetDto;
import com.userservice.exception.CustomException;
import com.userservice.model.User;

public interface UserService {

	public abstract User saveUser(User user) throws CustomException;

	public abstract List<User> getAllUsers() throws CustomException;

	public abstract User getUserById(Long id) throws CustomException;

	public abstract User updateUser(User user, Long id) throws CustomException;

	public abstract Boolean deleteUser(Long id) throws CustomException;

	List<PetDto> getMyPets(Long userId) throws CustomException;

	public abstract User buyPet(Long userId, Long petId) throws CustomException;

	public abstract String sayHello();

}
