package com.userservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.Data;



@Entity
@Table(name = "user")
@Data
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty(message = "Please enter valid username")
	@Size(min = 3,max = 15, message = "User Name must have between 3 to 14 characters")
	@Column(name = "user_name", nullable = false, unique = true, length = 15)
	private String userName;

	@NotEmpty(message = "Password is required")
	@Size(min = 4, max = 8, message = "Password must have between 4 to 8 characters")
	@Column(name = "user_password", nullable = false, length = 15)
	private String userPassword;

	@Transient
	@NotEmpty(message = "Confirm Password is required")
	@Size(min = 4, max = 8, message = "Confirm password must have between 4 to 8 characters")	
	@Column(name = "confirm_password", nullable = false, length = 15)
	private String confirmPassword;

}
