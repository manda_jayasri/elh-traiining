package com.petservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.petservice.exception.CustomException;
import com.petservice.model.Pet;
import com.petservice.model.User;
import com.petservice.service.PetService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(value = "pets")
@Slf4j
public class PetContoller {

	@Autowired
	private PetService petService;

	@GetMapping(value = "/hello")
	public ResponseEntity<String> sayHello() {
		String str = petService.sayHello();
		return new ResponseEntity<String>(str, HttpStatus.OK);
	}

	@PostMapping(value = "/create")
	public ResponseEntity<Pet> addPet(@RequestBody Pet pet) {
		log.info("Called Add Pet Successfully ");
		if (pet != null)
			try {
				petService.savePet(pet);
				log.info("Done Add Pet Successfully ");
				return new ResponseEntity<Pet>(pet, HttpStatus.CREATED);
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<Pet>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		return new ResponseEntity<Pet>(HttpStatus.BAD_REQUEST);
	}

	@GetMapping(value = "/getPet/{id}")
	public ResponseEntity<Pet> getPetById(@PathVariable("id") Long id) throws CustomException {
		log.info("Called serach Pet method ");
		Pet pet = petService.getPetById(id);
		if (pet != null) {
			log.info("Got Pet Successfully ");
			return new ResponseEntity<Pet>(pet, HttpStatus.OK);
		}
		return new ResponseEntity<Pet>(HttpStatus.NOT_FOUND);
	}

	@GetMapping(value = "/allPetsByUserId/{userId}")
	public ResponseEntity<List<Pet>> getAllPetsByUserId(@PathVariable Long userId) throws CustomException {
		log.info("Callet get all Pet by user Id methpd ");
		List<Pet> pets = petService.getAllPetsByUserId(userId);
		if (!pets.isEmpty()) {
			log.info("Found Pets data Successfully ");
			return new ResponseEntity<List<Pet>>(pets, HttpStatus.OK);
		}
		return new ResponseEntity<List<Pet>>(HttpStatus.NOT_FOUND);
	}

	@PutMapping(value = "/buyPet/{petId}")
	public ResponseEntity<Pet> buyPet(@PathVariable Long petId, @RequestBody User user) throws CustomException {
		log.info("Called Buy pet method..");
		Pet pet = petService.buyPet(user, petId);
		if (pet != null) {
			log.info("Bougt Pet to User Successfully ");
			return new ResponseEntity<Pet>(pet, HttpStatus.OK);
		}
		return new ResponseEntity<Pet>(HttpStatus.NOT_FOUND);
	}

	@GetMapping(value = "/allPets")
	public ResponseEntity<List<Pet>> getAllPets() throws CustomException {
		log.info("Callet get all Pet methpd ");
		List<Pet> pets = petService.getAllPets();
		if (!pets.isEmpty()) {
			log.info("Found Pets data Successfully ");
			return new ResponseEntity<List<Pet>>(pets, HttpStatus.OK);
		}
		return new ResponseEntity<List<Pet>>(HttpStatus.NOT_FOUND);
	}

	@PostMapping(value = "/updatePet/{id}")
	public ResponseEntity<Pet> updatePet(@RequestBody Pet pet, @PathVariable Long id) {
		log.info("Called update Pet ");
		if (pet != null && id != null)
			try {
				petService.updatePet(pet, id);
				log.info(" Update Pet Done Successfully ");
				return new ResponseEntity<Pet>(pet, HttpStatus.OK);
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<Pet>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		return new ResponseEntity<Pet>(HttpStatus.BAD_REQUEST);
	}

	@PostMapping(value = "/delete/{id}")
	public ResponseEntity<Boolean> removePet(@PathVariable Long id) {
		if (id != null)
			try {
				Boolean res = petService.deletePet(id);
				log.info(" Remove Pet Done Successfully ");
				return new ResponseEntity<Boolean>(res, HttpStatus.GONE);
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		return new ResponseEntity<Boolean>(HttpStatus.BAD_REQUEST);
	}

}
