package com.petservice.service;

import java.util.List;

import com.petservice.exception.CustomException;
import com.petservice.model.Pet;
import com.petservice.model.User;

public interface PetService {
	
	public abstract String sayHello();
	
	public abstract Pet savePet(Pet pet) throws CustomException;
	
	public abstract Pet buyPet(User user, Long petId) throws CustomException;
	
	public abstract List<Pet> getAllPetsByUserId(Long userId) throws CustomException;

	public abstract List<Pet> getAllPets() throws CustomException;

	public abstract Pet getPetById(Long id) throws CustomException;

	public abstract Pet updatePet(Pet pet, Long id) throws CustomException;

	public abstract Boolean deletePet(Long id) throws CustomException;

	

}
