package com.petservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.petservice.exception.CustomException;
import com.petservice.model.Pet;
import com.petservice.model.User;
import com.petservice.repository.PetRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PetServiceImpl implements PetService {
	@Autowired
	private PetRepository petRepository;

	@Override
	public String sayHello() {
		return "Welcome from Pet Service";
	}

	@Override
	public Pet savePet(Pet pet) throws CustomException {
		log.info("Called add new Pet in service Successfully ");
		Pet dbPet = petRepository.findByName(pet.getName());
		if (dbPet != null) {
			throw new CustomException(" Pet data already exists with name: " + pet.getName());
		}
		return petRepository.save(pet);
	}

	@Override
	public Pet buyPet(User user, Long petId) throws CustomException {

		Pet dbPet = petRepository.findById(petId).get();
		if (dbPet == null) {
			throw new CustomException(" Pet data doesnot exists with id: " + petId);
		}
		dbPet.setUser(user);

		return petRepository.save(dbPet);
	}

	@Override
	public List<Pet> getAllPetsByUserId(Long userId) throws CustomException {
		return (List<Pet>) petRepository.findByUser_Id(userId);
	}

	@Override
	public List<Pet> getAllPets() throws CustomException {
		log.info("Called find all Pets in service Successfully ");
		return (List<Pet>) petRepository.findAll();
	}

	@Override
	public Pet getPetById(Long id) throws CustomException {
		log.info("Called get Pet data in service ");
		Pet dbPet = petRepository.findById(id).get();
		if (dbPet == null) {
			throw new CustomException(" Pet data doesnot exists with id: " + id);
		}
		return dbPet;
	}

	@Override
	public Pet updatePet(Pet pet, Long id) throws CustomException {
		log.info("Called update Pet in service ");
		Pet dbPet = petRepository.findById(id).get();
		if (dbPet == null) {
			throw new CustomException(" Pet data doesnot exists with id: " + id);
		}
		dbPet.setName(pet.getName());
		dbPet.setPlace(pet.getPlace());
		dbPet.setAge(pet.getAge());
		return petRepository.save(dbPet);
	}

	@Override
	public Boolean deletePet(Long id) throws CustomException {
		log.info("Called delete Pet in service ");
		Pet dbPet = petRepository.findById(id).get();
		if (dbPet == null) {
			throw new CustomException(" Pet data doesnot exists with id: " + id);
		}
		petRepository.delete(dbPet);
		return true;
	}

}
