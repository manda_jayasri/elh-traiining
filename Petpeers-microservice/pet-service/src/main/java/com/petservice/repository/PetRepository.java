package com.petservice.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.petservice.model.Pet;

@Repository
public interface PetRepository extends CrudRepository<Pet, Long>{

	Pet findByName(String name);

	List<Pet> findByUser_Id(Long userId);

}
