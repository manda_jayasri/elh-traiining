package com.petservice.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.Data;

@Entity
@Table(name = "pets")
@Data
public class Pet implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Size(min = 3, message = "Pet Name must have at least 3 characters")
	@Column(name = "pet_name", nullable = false, length = 15)
	private String name;

	@NotEmpty(message = "Pet Place is required")
	@Column(name = "pet_place", nullable = false, length = 50)
	private String place;

	@Column(name = "pet_age", nullable = false, length = 3)
	private Integer age;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "owner_id")
	private User user;

}
