package com.hcl.pp.validator;

import org.springframework.stereotype.Component;

import com.hcl.pp.exception.UserException;

@Component
public class LognValidator {

	public boolean loginValidate(String userName, String userPassword) throws UserException {

		boolean res = false;
		if (userName != null && userPassword != null) {
			if (userName.length() >= 3 && userPassword.length() >= 4) {
				res = true;
			} else {
				throw new UserException("Enter Valid user Name & Password");
			}

		} else {
			throw new UserException("Enter User name & Password..");
		}

		return res;

	}

}
