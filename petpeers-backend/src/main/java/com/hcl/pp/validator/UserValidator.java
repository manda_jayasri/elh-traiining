package com.hcl.pp.validator;

import org.springframework.stereotype.Component;

import com.hcl.pp.exception.UserException;
import com.hcl.pp.model.User;

@Component
public class UserValidator {

	public boolean userValidate(User user) throws UserException {
		boolean result = true;

		if (user.getUserName() != null && user.getUserName().length() < 2 || user.getUserName().length() > 15) {
			throw new UserException("Enter Valid User Name");
		}

		if (user.getUserPassword() != null && user.getUserPassword().contains(" ")
				&& user.getUserPassword().length() < 3 || user.getUserPassword().length() > 9) {
			throw new UserException("Enter Valid User Password");

		}

		System.out.println("Password :" + user.getUserPassword() + " C_Password: " + user.getConfirmPassword());
		if (!user.getUserPassword().equals(user.getConfirmPassword())) {
			throw new UserException("Confirm Password is not matched");
		}

		return result;
	}

}
