package com.hcl.pp.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Manda Jayasri
 */

@Entity
@Table(name = "PET_USER ")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty(message = "Please enter valid username")
	@Size(min = 3,max = 15, message = "User Name must have between 3 to 14 characters")
	private String userName;

	@NotEmpty(message = "Password is required")
	@Size(min = 4, max = 8, message = "Password must have between 4 to 8 characters")
	@Column(length = 12)
	private String userPassword;

	@NotEmpty(message = "Confirm Password is required")
	@Size(min = 4, max = 8, message = "Confirm password must have between 4 to 8 characters")
	@Column(length = 12)
	private String confirmPassword;

	@JsonIgnore
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	private Set<Pet> pets;

}
