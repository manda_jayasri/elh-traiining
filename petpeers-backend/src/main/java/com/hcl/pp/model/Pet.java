package com.hcl.pp.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 
 * @author Manda Jayasri
 */

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "PETS")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Pet implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty(message = "Pet Name is required")
	@Size(min = 3, message = "Pet Name must have at least 3 characters")
	@Column(name = "pet_name")
	private String name;

	@NotEmpty(message = "Pet Place is required")
	@Column(name = "pet_place")
	private String place;

	@NotNull(message = "Pet Age is required")
	@Column(name = "pet_age")
	private Integer age;
	
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "PET_OWNERID")
	private User user;

}
