package com.hcl.pp.service;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.pp.exception.PetException;
import com.hcl.pp.exception.UserException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.model.User;
import com.hcl.pp.repository.PetRepository;
import com.hcl.pp.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PetRepository petRepository;

	@Override
	public User addUser(User user) throws UserException {
		User dbUser = userRepository.findByUserName(user.getUserName());
		if (dbUser != null) {
			throw new UserException(" User data already exists with name: " + user.getUserName());
		}
		return userRepository.save(user);
	}

	@Override
	public List<User> listUsers() throws UserException {
		return (List<User>) userRepository.findAll();
	}

	@Override
	public User authenticateUset(String userName, String userPasswd) throws UserException {
		User dbUser = userRepository.findByUserNameAndUserPassword(userName, userPasswd);
		if (dbUser == null) {
			throw new UserException(
					" User data doesn't exist with given Username: " + userName + " & Password: " + userPasswd);
		}

		return dbUser;
	}

	@Override
	public Set<Pet> getMyPets(Long userId) throws UserException {
		User dbUser = userRepository.findById(userId).get();
		if (dbUser == null) {
			throw new UserException(" User data doesnot exists with id: " + userId);
		}
		Set<Pet> pets = new TreeSet<Pet>();
		pets = dbUser.getPets();
		return pets;
	}

	@Override
	public User buyPet(Long userId, Long petId) throws PetException, UserException {
		User dbUser = userRepository.findById(userId).get();
		if (dbUser == null) {
			throw new UserException(" User data doesn't exists with id: " + userId);
		}
		Pet dbPet = petRepository.findById(petId).get();
		if (dbPet == null) {
			throw new PetException(" Pet data doesnot exists with id: " + petId);
		}

		dbPet.setUser(dbUser);
		petRepository.save(dbPet);
		return dbUser;
	}

	@Override
	public User getUserById(Long userId) throws UserException {
		User dbUser = userRepository.findById(userId).get();
		if (dbUser == null) {
			throw new UserException(" User data doesnot exists with id: " + userId);
		}
		return dbUser;
	}

	@Override
	public User getUserByName(String userName) throws UserException {
		User dbUser = userRepository.findByUserName(userName);
		if (dbUser == null) {
			throw new UserException(" User data doesnot exists with name: " + userName);
		}
		return dbUser;
	}

	@Override
	public Boolean removeUser(Long userId) throws UserException {
		User dbUser = userRepository.findById(userId).get();
		if (dbUser == null) {
			throw new UserException(" User data doesnot exists with id: " + userId);
		}
		userRepository.delete(dbUser);
		return true;
	}

	@Override
	public User updateUser(User user, Long userId) throws UserException {
		User dbUser = userRepository.findById(userId).get();
		if (dbUser == null) {
			throw new UserException(" User data doesnot exists with id: " + userId);
		}
		dbUser.setUserName(user.getUserName());
		dbUser.setUserPassword(user.getUserPassword());
		dbUser.setConfirmPassword(user.getConfirmPassword());
		return userRepository.save(dbUser);
	}

}
