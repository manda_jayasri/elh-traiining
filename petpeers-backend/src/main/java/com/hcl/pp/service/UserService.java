package com.hcl.pp.service;

import java.util.List;
import java.util.Set;

import com.hcl.pp.exception.PetException;
import com.hcl.pp.exception.UserException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.model.User;

public interface UserService {

	public abstract User addUser(User user) throws UserException;

	public abstract User authenticateUset(String userName,String userPasswd) throws UserException;

	public abstract Boolean removeUser(Long userId) throws UserException;

	public abstract List<User> listUsers() throws UserException;

	public abstract Set<Pet> getMyPets(Long userId) throws UserException;

	public abstract User buyPet(Long userId, Long petId) throws PetException, UserException;

	public abstract User getUserById(Long userId) throws UserException;

	public abstract User getUserByName(String userName) throws UserException;

	public abstract User updateUser(User user, Long userId) throws UserException;

}
