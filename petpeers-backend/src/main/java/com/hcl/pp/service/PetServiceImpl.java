package com.hcl.pp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.pp.exception.PetException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.repository.PetRepository;

@Service
public class PetServiceImpl implements PetService {

	@Autowired
	private PetRepository petRepository;


	@Override
	public Pet savePet(Pet pet) throws PetException {
		Pet dbPet = petRepository.findByName(pet.getName());
		if (dbPet != null) {
			throw new PetException(" Pet data already exists with name: " + pet.getName());
		}
		return petRepository.save(pet);
	}

	@Override
	public List<Pet> findAllPets() throws PetException {
		return (List<Pet>) petRepository.findAll();
	}

	@Override
	public Pet getPetById(Long petId) throws PetException {
		Pet dbPet = petRepository.findById(petId).get();
		if (dbPet == null) {
			throw new PetException(" Pet data doesnot exists with id: " + petId);
		}
		return dbPet;
	}

	@Override
	public Pet getPetByName(String petName) throws PetException {
		Pet dbPet = petRepository.findByName(petName);
		if (dbPet == null) {
			throw new PetException(" Pet data doesnot exists with name: " + petName);
		}
		return dbPet;
	}

	@Override
	public Boolean deletePetById(Long petId) throws PetException {
		Pet dbPet = petRepository.findById(petId).get();
		if (dbPet == null) {
			throw new PetException(" Pet data doesnot exists with id: " + petId);
		}
		petRepository.delete(dbPet);
		return true;
	}

	@Override
	public Pet updatePet(Pet pet, Long petId) throws PetException {
		Pet dbPet = petRepository.findById(petId).get();
		if (dbPet == null) {
			throw new PetException(" Pet data doesnot exists with id: " + petId);
		}
		dbPet.setName(pet.getName());
		dbPet.setPlace(pet.getPlace());
		dbPet.setAge(pet.getAge());
		return petRepository.save(dbPet);
	}

}
