package com.hcl.pp.service;

import java.util.List;

import com.hcl.pp.exception.PetException;
import com.hcl.pp.model.Pet;

public interface PetService {
	
	public abstract Pet savePet(Pet pet) throws PetException;

	public abstract List<Pet> findAllPets() throws PetException;

	public abstract Pet getPetById(Long petId) throws PetException;

	public abstract Pet getPetByName(String petName) throws PetException;

	public abstract Boolean deletePetById(Long petId) throws PetException;

	public abstract Pet updatePet(Pet pet, Long petId) throws PetException;

}
