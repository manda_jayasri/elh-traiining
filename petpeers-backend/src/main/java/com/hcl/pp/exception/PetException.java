package com.hcl.pp.exception;

public class PetException extends Exception {

	private String message;

	public PetException() {
		super();
	}

	public PetException(String message) {
		super();
		this.message = message;
	}

	@Override
	public String getMessage() {
		return this.message;
	}

}
