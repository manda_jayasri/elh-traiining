package com.hcl.pp.exception;

public class UserException extends Exception {

	private String message;

	public UserException() {
		super();
	}

	public UserException(String message) {
		super();
		this.message = message;
	}

	@Override
	public String getMessage() {
		return this.message;
	}

}
