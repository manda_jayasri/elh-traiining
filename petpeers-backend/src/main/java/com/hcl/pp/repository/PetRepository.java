package com.hcl.pp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hcl.pp.model.Pet;

@Repository
public interface PetRepository extends CrudRepository<Pet, Long>{

	Pet findByName(String petName);

}
