package com.hcl.pp.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.pp.exception.PetException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.service.PetService;
import com.hcl.pp.service.UserServiceImpl;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(value = "pets")
public class PetController {

	private static final Logger LOGGER = LogManager.getLogger(PetController.class);

	@Autowired
	private PetService petService;

	@PostMapping(value = "addPet")
	public Pet savePet(@Valid @RequestBody Pet pet) throws PetException {
		LOGGER.info("Called Add Pet method..");
		return petService.savePet(pet);
	}

	@RequestMapping(value = "allPets")
	public List<Pet> petHome() throws PetException {
		LOGGER.info("Fetching All Pets..");
		return petService.findAllPets();
	}

	@GetMapping(value = "petDetail/{id}")
	public Pet getPetById(@Valid @PathVariable("id") Long petId) throws PetException {
		LOGGER.info("Called Fetch Pet by id method..");
		return petService.getPetById(petId);
	}

	@PutMapping(value = "updatePetDetails/{petId}")
	public Pet updatePet(@Valid @RequestBody Pet pet, @Valid @PathVariable Long petId) throws PetException {
		LOGGER.info("Called Update Pet method..");
		return petService.updatePet(pet, petId);
	}

	@DeleteMapping(value = "deletePetDetail/{petId}")
	public Boolean deletePetById(@Valid @PathVariable Long petId) throws PetException {
		LOGGER.info("Called Delete Pet method..");
		return petService.deletePetById(petId);
	}

	@GetMapping(value = "getByname/{name}")
	public Pet getPetByName(@Valid @PathVariable("name") String petName) throws PetException {
		return petService.getPetByName(petName);
	}

}
