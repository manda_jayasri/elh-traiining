package com.hcl.pp.controller;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.pp.exception.PetException;
import com.hcl.pp.exception.UserException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.model.User;
import com.hcl.pp.service.UserService;
import com.hcl.pp.validator.LognValidator;
import com.hcl.pp.validator.UserValidator;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(value = "user")
public class UserController {

	private static final Logger LOGGER = LogManager.getLogger(UserController.class);

	@Autowired
	private UserService userService;

	@Autowired
	private LognValidator loginvalidator;

	@Autowired
	private UserValidator userValidator;

	@PostMapping(value = "add")
	public User addUser(@Valid @RequestBody User user) throws UserException {
		LOGGER.info("Called Add User method..");
		boolean result = userValidator.userValidate(user);
		if (result == false) {
			throw new UserException();
		}
		return userService.addUser(user);
	}

	@GetMapping(value = "login/")
	public User loginUser(@Valid @RequestParam(value = "userName", required = true) String userName,
			@Valid @RequestParam(value = "userPasswd", required = true) String userPasswd) throws UserException {
		LOGGER.info("Called login method");
		boolean result = loginvalidator.loginValidate(userName, userPasswd);
		if (result == false) {
			throw new UserException();
		}
		return userService.authenticateUset(userName, userPasswd);
	}

	@RequestMapping(value = "listUsers")
	public List<User> findAllusers() throws UserException {
		LOGGER.info("Called fetch all user method in controller");
		return userService.listUsers();
	}

	@DeleteMapping(value = "delete/{userId}")
	public Boolean removeUser(@Valid @PathVariable Long userId) throws UserException {
		LOGGER.info("Called delete User method..");
		return userService.removeUser(userId);
	}

	@GetMapping(value = "pets/myPets/{userId}")
	public Set<Pet> getMyPets(@PathVariable Long userId) throws UserException {
		LOGGER.info("Called get Pet by User id..");
		return userService.getMyPets(userId);
	}

	@GetMapping(value = "pets/buyPet/{userId}/{petId}")
	public User buyPet(@PathVariable Long userId, @PathVariable Long petId) throws PetException, UserException {
		LOGGER.info("Called Buy pet method..");
		return userService.buyPet(userId, petId);
	}

	@PutMapping(value = "update/{userId}")
	public User updateUser(@Valid @RequestBody User user, @PathVariable Long userId) throws UserException {
		return userService.updateUser(user, userId);
	}

	@GetMapping(value = "getById/{id}")
	public User getUserById(@Valid @PathVariable("id") Long userId) throws UserException {
		return userService.getUserById(userId);
	}

	@GetMapping(value = "getByname/{name}")
	public User getUserByName(@Valid @PathVariable("name") String userName) throws UserException {

		return userService.getUserByName(userName);
	}

}
